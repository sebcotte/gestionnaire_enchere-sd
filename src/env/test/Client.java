package env.test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;


/**
 * Client se connecte a un serveur
 * Cette classe va envoyer des donnees au serveur (pas de donnees saisies au clavier)
 *
 */
public class Client implements Runnable{
	//comit test Nadia
	private String hote;
	private int port;
	private int msg_delay;
	private int nb_msg_limit;
	
	public Client(String hote, int port, int msg_delay, int nb_msg_limit){
		this.hote = hote;//"127.0.0.1";
		this.port = port;//12000;
		this.msg_delay = msg_delay;
		this.nb_msg_limit = nb_msg_limit;
	}
	
	/**
	 * Lit un fichier qui contient des actions � faire et les envoie au serveur
	 * @param f : le fichier qui contient les actions
	 */
	public void sendActions(Socket connexion, File f){
		try {
			
			PrintWriter out = new PrintWriter(connexion.getOutputStream(), true);
			System.out.format(" Contacting %s on %d\n", hote ,port);
			
			// Parcours du fichier
			
			//out.println(donneesDuFichier);
			out.println("ATTENDRE");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*public void receiveDataFromServer(Socket connexion){
		try {
			// Recupere le msg du serveur
			InputStream in = connexion.getInputStream();			
			String infoFromServer = in.toString();
			
			// Affiche ce msg sur la console du client
			PrintWriter out = new PrintWriter(connexion.getOutputStream(), true);
			out.println(" ---- "+ infoFromServer + "-------");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/
	


	@Override
	public void run() {	
		
		Socket laConnection = null;
		try {
			// Se connecte a un serveur
			laConnection = new Socket(this.hote, this.port);
		
			// Envoie des donnees a un serveur
			for(int i = 0 ; i < this.nb_msg_limit ; i++) {
				
				/** Envoyer une donnee venant au serveur grace au canal OutputStream **/
				this.sendActions(laConnection, null);
				
				Thread.sleep(this.msg_delay); // On attend avant de renvoyer une donn�e
				
				//this.receiveDataFromServer(laConnection);
			}
	
			
		} catch (IOException | InterruptedException e) {
			System.out.format("Probleme de connection avec serveur fontionne : %s",e);
			System.exit(-1);
		}		
	}
}
