package env.test;


/**
 * 
 * ArreterServeur permet l'arret du serveur principal
 * Cette classe a �t� cr��e pour forcer toutes les copies locales de SERviceClient a mettre a jour 
 * leur attribut "arreterServeur"
 *
 */
public class ArreterServeur {

	private boolean arreter;
	
	public ArreterServeur(){
		
		this.arreter = false;
	}
	
	public synchronized void setTrue(){
		arreter = true;
	}
	
	public synchronized void setFalse(){
		arreter = false;
	}
	
	public synchronized boolean getValue(){
		return arreter;
	}
}
