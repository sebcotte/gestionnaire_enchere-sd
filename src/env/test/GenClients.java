package env.test;



/**
 * Cette classe remplace l'invite de commande avec telnet
 *
 */

public class GenClients {

	public static void main(String[] args) {

		Client client;
		String hote = "127.0.0.1";
		int port = 12000;
		int msg_delay = 2000;
		int nb_msg_limit = 10;
		
		for(int i = 0; i < 5 ; i++){
			client = new Client(hote, port, msg_delay, nb_msg_limit);
			Thread thread = new Thread(client);
			
			thread.start();
		}
	}

}
