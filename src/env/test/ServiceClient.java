package env.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;


/**
 * Cette classe est utilis�e lorsqu'un client se connecte au serveur. 
 * Elle permet de servir le client connect�
 * **/
public class ServiceClient implements Runnable{
	
	private Socket la_connection;
	private int num_port;
	private ArreterServeur arreter;
	private int timeout; // si pendant timeout milliseconde le client est inactif alors on ferme la connexion
	
	public ServiceClient(Socket connection, int num_port, int timeout){
		this.la_connection = connection;
		this.num_port = num_port;
		this.arreter = new ArreterServeur(); // arreter = false
		this.timeout = timeout;
	}

	@Override
	public void run() {
		
		try {			
			// SocketTimeoutException
			la_connection.setSoTimeout(this.timeout); // Si un client ne fait rien pendant x sec, on ferme la socket
			
			/* On Associe une file d'entr�e a la connection */
	        InputStreamReader isr = new InputStreamReader(la_connection.getInputStream());
	        /* On transforme cette file en file avec tampon */
	        BufferedReader flux_entrant = new BufferedReader(isr);
	        System.out.println("Tampon entree attache ");
	        //PrintWriter ma_sortie = new PrintWriter(la_connection.getOutputStream(), true);
	        System.out.println("Sortie attach�e");
	        System.out.println("Pr�t � servir le Client : "+ la_connection.getRemoteSocketAddress());
	
	        String clientName = la_connection.getRemoteSocketAddress().toString();
	        String message_lu = new String();
	        int line_num = 0;
	
	        /*
	         * On lit le flux_entrant ligne � ligne ATTENTION : La fonction readline
	         * est Bloquante readline retourne null si il y a souci avec la
	         * connexion On s arrete aussi si connexion_non_terminee est vraie
	         */
	        //ma_sortie.format("Bonjour %s j attends tes donnees  \n",clientName);
	        
			while(  (message_lu = flux_entrant.readLine()) != null )   {
			        System.out.format("%d: ->  [%s]\n", line_num, message_lu);
			        line_num++;
			        /* si on recoit Finish on clot et annonce cette terminaison */
			        if (message_lu.contains("Finish")) {
			                System.out.println("Reception de  " + "Finish"
			                                + " -> Transmission finie");
			                // On ferme la connection
			                System.out.format("Je clos la connection  %s :\n",clientName);
			                terminer(la_connection);
			        }
			        else if(message_lu.equals("ATTENDRE")){
			        	// Faire l'action, envoyer donnee au serveur
			        	System.out.println("ServiceCLient parle au serveur !!");
			        }
			}
		} catch (SocketException e) {
			System.out.println("Connexion termin�e [" + e.toString() + "]");
		} catch (SocketTimeoutException timeout) {
			terminer(la_connection);
			System.out.println("Client inactif ! [timeout > " + this.timeout + "] [" + timeout.toString() + "]");
		} catch(IOException e) {
			System.out.println("Erreur entr�e sortie [" + e.toString() +"]");
		}
	}
	
	public void terminer(Socket la_connection){
		this.arreter.setTrue();
		
		if (la_connection != null)       	
	    {
		    try 	{
				la_connection.close(); 
				System.out.format("Socket fermee \n"); 
		    }
		    catch ( IOException e )   { 
		    	System.out.println("Weird, nawak .... \n ");
		    }    
		}
	}
}
