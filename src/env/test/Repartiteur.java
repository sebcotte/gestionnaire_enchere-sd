package env.test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


/**
 * Repartiteur est un serveur qui attend que des clients se connectent et peut les accepter
 * Lorsqu'un client se connecte elle creer un thread qui va service ce dernier
 * **/
public class Repartiteur {
	
	private int port;
	private ServerSocket mon_connecteur;
	private int timeout;
	
	public Repartiteur(int port, int timeout) throws IOException{
		this.port = port;
		this.mon_connecteur = new ServerSocket(port);
		this.timeout = timeout;
		System.out.format("Serveur  lanc� sur le  port %d\n", port);
	}
	
	public ServerSocket getConnecteur(){
		return this.mon_connecteur;
	}
	
	public int getTimeout(){
		return this.timeout;
	}

	public static void main(String[] args) throws IOException{
		
		Socket ma_connection = null;
		
		Repartiteur rep = new Repartiteur(12000, 6000); // 6s c'est le temps que le serveur attend le client avant de fermer
		
		while(true){
			 // On attend un client
			ma_connection = rep.getConnecteur().accept();
			
			int c_port = ma_connection.getPort();
			String c_ip = ma_connection.getInetAddress().toString();
			System.out.format("Un client est arriv� avec IP : %s sur le port %d\n", c_ip, c_port);
			
			/* On traite le client que l'on a associ� et on demmarre un thread*/
			ServiceClient service_client = new ServiceClient(ma_connection, c_port, rep.getTimeout());
			Thread thread_client = new Thread(service_client);
			thread_client.start();
		}
	}
}
